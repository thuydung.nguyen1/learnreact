import './App.css';
import axios from 'axios'
import { useEffect, useState } from 'react';

function App() {
  const [update, setUpdate] = useState(false)
  const [posts, setPosts] = useState([]);
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const userToPatch = {
    title: 'foo',
  };
  var newListPost = [];

  const getAll = () => {
    axios.get("https://jsonplaceholder.typicode.com/posts").then((res) => {
      setPosts(res.data)
      // console.log(res.data)
    })
  };
  useEffect(() => {
    getAll();
    setUpdate(false);
  }, [update]);

  const postData = (e) => {
    e.preventDefault();
    axios.post("https://jsonplaceholder.typicode.com/posts", { title, body }).then(res => {
      // newListPost.push(res.data)
      console.log(res.data)
      newListPost = [...posts, res.data]
      console.log(newListPost)
      setPosts(newListPost)
      setTitle('');
      setBody('');
    })
    setUpdate(true)
  }

  const deletePost = (e) => {
    const id = e.target.value;
    axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`).then(res => {
      console.log(res.data)
      // alert("Delete Successfully")
      for (let i = 0; i <= posts.length; i++) {
        if (posts[i]?.id == id) {
          posts.splice(i, 1);
          // posts.splice(i, 0);
          console.log(posts);
          setPosts(posts);
        }
      }
    })
    setUpdate(true)
  }


  const patchData = (e) => {
    const id = e.target.value;
    axios.patch(`https://jsonplaceholder.typicode.com/posts/${id}`, userToPatch).then(res => {
      console.log(res)
      for (let i = 0; i <= posts.length; i++) {
        if (posts[i]?.id == id) {
          posts[i] = res.data
          // posts.splice(i, 0);
          console.log(posts);
          setPosts(posts);
        }
      }
    })
    console.log(posts);
    setPosts(posts)
    setUpdate(true)
  }

  const putData = (e) => {
    const id = e.target.value;
    axios.put(`https://jsonplaceholder.typicode.com/posts/${id}`, userToPatch).then(res => {
      for (let i = 0; i <= posts.length; i++) {
        if (posts[i]?.id == id) {
          posts[i] = res.data
          // posts.splice(i, 0);
          console.log(posts);
          setPosts(posts);
        }
      }
    })
    setUpdate(true)
  }

  return (
    <div className="App">
      <div>POST Data</div>
      <form>
        <label>Title</label>
        <input type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
        <label>Body</label>
        <input type="text" value={body} onChange={(e) => setBody(e.target.value)} />
        <button onClick={postData}>POST</button>

      </form>

      <table>
        <thead>
          <tr  >
            <th>ID</th>
            <th>Title</th>
            <th>Body</th>
            <th>Action</th>
          </tr>
        </thead>

        <tbody>
          {posts.map((post, index) => (
            <tr key={index}>
              <td>{post.id}</td>
              <td>{post.title}</td>
              <td>{post.body}</td>
              <td>
                <button onClick={deletePost} value={post.id}>Delete</button>
                <button onClick={putData} value={post.id}>Put</button>
                <button onClick={patchData} value={post.id}>Edit</button>
              </td>
            </tr>
          ))}
        </tbody>

      </table>



    </div>
  );
}

export default App;
